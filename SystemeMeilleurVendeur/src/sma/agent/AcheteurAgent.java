 package sma.agent;

import java.util.ArrayList;

import com.google.gson.Gson;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ParallelBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.ControllerException;
import sma.AcheteurContainer;
import sma.Produit;

public class AcheteurAgent extends GuiAgent{
	//on cree un ArrayList de type Objet
	ArrayList<Produit> produitMin=new ArrayList<Produit>();
	//on initialise l'Array(null,300,null)
	Produit produit12=new Produit("",300,"");
	
	private AcheteurContainer gui;
	private AID[] lstVendeur;
	@Override
	protected void setup() {
		gui=(AcheteurContainer) getArguments()[0];
		gui.setAcheteurAgent(this);
		System.out.print("Initialisation de l'agent "+this.getAID().getName());
		produitMin.add(produit12);
		
		ParallelBehaviour parallelBehaviour=new ParallelBehaviour();
		addBehaviour(parallelBehaviour);
		parallelBehaviour.addSubBehaviour(new TickerBehaviour(this,6000) {
			//recherche de service---------------------------
			@Override
			protected void onTick() {
				try {
					DFAgentDescription dfa=new DFAgentDescription();
					ServiceDescription sd=new ServiceDescription();
					sd.setType("Vente");
					dfa.addServices(sd);
					DFAgentDescription[] agentDescription=DFService.search(myAgent, dfa);
					lstVendeur=new AID[agentDescription.length];
					for(int i=0;i<agentDescription.length;i++) {
						lstVendeur[i]=agentDescription[i].getName();
					}
				} catch (FIPAException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		parallelBehaviour.addSubBehaviour(new CyclicBehaviour(){

			@Override
			public void action() {
				
				ACLMessage message=receive();
				
				if(message!=null) {
					
					switch (message.getPerformative()) {
					//reception de message de type REUEST
					case ACLMessage.REQUEST:
//					
					System.out.println("Reception d'un message "+message.getContent());
					GuiEvent guiEvent=new GuiEvent(this, 1);
					String nomProduit=message.getContent();
					guiEvent.addParameter(message.getContent());
					gui.viewMessage(guiEvent);
					/*
					 * Operation de l'achat produit
					 * */
					//envoie de message CFP a tout les vendeurs apres le REQUEST
					ACLMessage aclMessage=new ACLMessage(ACLMessage.CFP);
					
					aclMessage.setContent(nomProduit);
					for(AID aid:lstVendeur) {
						aclMessage.addReceiver(aid);
						System.out.println(" AID message envoye");
						System.out.println(aid);
					}
					send(aclMessage);
					
					break;
					/*Reception de message de type PROPOSE envoye par les agents Vendeur apres qu'ils ont recu
					 * le CFP de l'agent Acheteur*/
					case ACLMessage.PROPOSE:
						
						Gson gson = new Gson();
						Produit produit = gson.fromJson(message.getContent(), Produit.class);
						System.out.println("proposition de produit");
						
						//on reccupere les champs de l'objet produit envoye en Json
						String n=produit.getName();
						int p=produit.getPrice();
						String s=message.getSender().toString();
						AID id=message.getSender();
						//on affiche les propositions de produit dans le terminal
						System.out.println(n+"|"+p);
						//on reccupere chaque proposition d'un vendeur on cree un nouvel objet produit
						Produit produit1=new Produit(n,p,s);
						
						
						/*au depart nous avons cree un arrayList produitMin initialise avec (null,300,null) respectivement
						 * name,price,sender
						 * on sait que le prix d'un produit ne va pas depasser 300 dans notre cas parce que le random
						 * qui cree le prix des produits chez l'agent vendeur est de range 200
						 * l'agent acheteur fait un test a chaque proposition de produit il compare le prix a celui du produit qui existait 
						 * deja dans l'arrayList
						 * et il garde le produit avec le prix minimal
						 * */
						
						if(produitMin.get(0).getPrice()>=p) {
						produitMin.add(0,produit1);
							
						
						}
						/*on sait que tout les agents vendeurs ont le meme produit dans notre cas seul les prix sont varies
						 * l'agent acheteur attent la proposition de tout les vendeurs quand k est egal a la derniere indice du tableau
						 * de la liste des vendeurs apres avoir recu toutes les propositions il envoie un message ACCEPT_PROPOSAL
						 * au vendeur qui detient le produit le plus bon marche et il INFORM le consommateur de son choix
						 * 
						 * */
						 
						for(int k=0; k<lstVendeur.length; k++) {
							if ((lstVendeur[k].equals(id)) && (k==lstVendeur.length-1)) {
								System.out.println("produit Min");
								System.out.println(produitMin.get(0).getName()+" "+produitMin.get(0).getPrice()+" "+produitMin.get(0).getSender());
								
								ACLMessage aclMessage1=new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
								aclMessage1.setContent("Jaccepte ta proposition");
								aclMessage1.addReceiver(lstVendeur[k]);
								send(aclMessage1);
								
								ACLMessage aclMessage2=new ACLMessage(ACLMessage.INFORM);
								aclMessage2.setContent(lstVendeur[k]+"%"+produitMin.get(0).getName()+" "+produitMin.get(0).getPrice());
								aclMessage2.addReceiver(new AID("consomateur",AID.ISLOCALNAME));
								send(aclMessage2);
								
								produitMin.clear();
								
								
							}
						}
						
						break;
						
						
					
					
				}
					
			}
			}
			
			
			
			
			
		});
	}
	@Override
	protected void takeDown() {
		System.out.println("Desctruction de l'agent");
	}

	@Override
	protected void beforeMove() {
		try {
			System.out.println("Avant migration ... du container "+this.getContainerController().getContainerName());
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void afterMove() {
			try {
				System.out.println("Apres migration ... vers le container "+this.getContainerController().getContainerName());
			} catch (ControllerException e) {
				e.printStackTrace();
			}
		}
	@Override
	public void onGuiEvent(GuiEvent guiEvent) {

	}
}



