package sma.agent;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.ControllerException;
import sma.ConsomateurContainer;
import sma.Note;

import java.util.ArrayList;

public class ConsomateurAgent extends GuiAgent{
	ArrayList<Note> listNote=new ArrayList<Note>();
	private ConsomateurContainer gui;
	@Override
	protected void setup() {
		gui=(ConsomateurContainer) getArguments()[0];
		gui.setConsomateurAgent(this);
		System.out.print("Initialisation de l'agent "+this.getAID().getName());
		
		addBehaviour(new CyclicBehaviour(){

			@Override
			public void action() {
				MessageTemplate messageTemplate=MessageTemplate.MatchPerformative(ACLMessage.INFORM);
				ACLMessage message=receive();
				if(message!=null) {
					
					System.out.println("Reception d'un message "+message.getContent());
					String [] messageR =message.getContent().split("%");
					GuiEvent guiEvent=new GuiEvent(this, 1);
					guiEvent.addParameter(messageR[1]);
					guiEvent.addParameter(messageR[0]);
					//guiEvent.addParameter(message.getContent());
					gui.viewMessage(guiEvent);
				}
				
			}
			
		}); 
	}
	@Override
	protected void takeDown() {
		System.out.println("Desctruction de l'agent");
	}

	@Override
	protected void beforeMove() {
		try {
			System.out.println("Avant migration ... du container "+this.getContainerController().getContainerName());
		} catch (ControllerException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void afterMove() {
			try {
				System.out.println("Apres migration ... vers le container "+this.getContainerController().getContainerName());
			} catch (ControllerException e) {
				e.printStackTrace();
			}
		}
	@Override
	public void onGuiEvent(GuiEvent guiEvent) {
		if(guiEvent.getType()==1) {
			ACLMessage aclMessage=new ACLMessage(ACLMessage.REQUEST);
			String produit=guiEvent.getParameter(0).toString();
			aclMessage.setContent(produit);
			aclMessage.addReceiver(new AID("acheteur",AID.ISLOCALNAME));
			send(aclMessage);
		} 
	}
	
//	public void onGuiEvent2(GuiEvent guiEvent2) {
//		if(guiEvent2.getType()==2) { 
//			MessageTemplate messageTemplate=MessageTemplate.MatchPerformative(ACLMessage.INFORM);
//			ACLMessage message=receive();
//			String [] messageR =message.getContent().split(":");
//			String note=guiEvent2.getParameter(0).toString();
//			double note1= Double.parseDouble(note);
//			Note noteAgent=new Note("mike", note1);
//			listNote.add(noteAgent);
//			System.out.print(listNote.get(0).getSender());
//		}
//	}
	
}



