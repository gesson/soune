package sma;

public class Produit {
	private String vendeur;
	private int price;
	private String name;
	private String sender;
	
	
	public Produit(String pName, int pPrice) {
		name=pName;
		price=pPrice;
		
	}
	public Produit(String pName, int pPrice, String pSender) {
		name=pName;
		price=pPrice;
		sender=pSender;
		
		
	}
	
	
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price=price;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVendeur() {
		return vendeur;
	}
	public void setVendeur(String name) {
		this.vendeur = vendeur;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String name) {
		this.sender = sender;
	}
	
}
