package sma;

import java.util.List;
import java.util.Random;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.gui.GuiEvent;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sma.agent.ConsomateurAgent;
import sma.agent.VendeurAgent;

public class VendeurContainer extends Application{
	private VendeurAgent vendeurAgent;
	private ObservableList<String> observableList;
	private AgentContainer agentContainer;
	private VendeurContainer vendeurContainer;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(VendeurContainer.class);
	}

	public void startContainer() {
		try { 
			Runtime runtime=Runtime.instance();
			Profile profile=new ProfileImpl(false);
			profile.setParameter(Profile.MAIN_HOST, "localhost");
			agentContainer=runtime.createAgentContainer(profile);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		startContainer();
		vendeurContainer=this;
		primaryStage.setTitle("Vendeur");
		BorderPane borderPane=new BorderPane();
		
		HBox hBox=new HBox();
		hBox.setPadding(new Insets(10));
		hBox.setSpacing(10);
		Label labelVendeur=new Label("Nom du vendeur:");
		TextField txtVendeur=new TextField();
		Button btnVendeur=new Button("Deployer vendeur");
		hBox.getChildren().add(labelVendeur);
		hBox.getChildren().add(txtVendeur);
		hBox.getChildren().add(btnVendeur);
		borderPane.setTop(hBox);
		VBox vBox=new VBox();
		GridPane gridPane=new GridPane();
		
		observableList=FXCollections.observableArrayList();
		ListView<String> lstViewMessage=new ListView<String>(observableList);
		
		gridPane.add(lstViewMessage, 0, 0);
		vBox.setPadding(new Insets(10));
		vBox.setSpacing(10);
		vBox.getChildren().add(gridPane);
		borderPane.setCenter(vBox);
		Scene scene=new Scene(borderPane,400,500);
		primaryStage.setScene(scene);
		primaryStage.show();
		
		btnVendeur.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				
				
				
				try {
					String nomVendeur=txtVendeur.getText();
					AgentController agentController=agentContainer.createNewAgent
							(nomVendeur, "sma.agent.VendeurAgent", new Object[] {vendeurContainer});
					agentController.start();
					
					Random rand =new Random();
					Random rand1 =new Random();
					Random rand2 =new Random();
					int n=rand.nextInt(200);
					int n1=rand1.nextInt(200);
					int n2=rand1.nextInt(200);
					int t[]= {n,n1,n2};
					
//					Produit produit= new  Produit("java xml", t[0]);
//					Produit produit1=new Produit("Romero et juliette", t[1]);
//					Produit produit2=new Produit("Perle des antilles", t[2]);
//					
					observableList.add("Liste des produits");
//					observableList.add(produit.getName()+" "+produit.getPrice());
//					observableList.add(produit1.getName()+" "+produit1.getPrice());
//					observableList.add(produit2.getName()+" "+produit2.getPrice());
					
					//System.out.println(t[1]);
				} catch (StaleProxyException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		});
	}
	
	public VendeurAgent getVendeurAgent() {
		return vendeurAgent;
	}

	public void setVendeurAgent(VendeurAgent vendeurAgent) {
		this.vendeurAgent = vendeurAgent;
	}

	public void viewMessage(GuiEvent guiEvent) {
		String message=guiEvent.getParameter(0).toString();
		observableList.add(message);
	}
	
	
}
